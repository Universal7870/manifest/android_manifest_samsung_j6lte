# Skyhawk Recovery Project

### How to build ###

```bash
# Create dirs
$ mkdir skyhawk ; cd skyhawk

# Init repo
$ repo init --depth=1 -u git://github.com/SKYHAWK-Recovery-Project/platform_manifest_twrp_omni.git -b 9.0

# Clone my local repo
$ git clone https://gitlab.com Universal7870/manifest/android_manifest_samsung_j6lte.git -b skyhawk .repo/local_manifests

# Sync
$ repo sync --no-repo-verify -c --force-sync --no-clone-bundle --no-tags --optimized-fetch --prune -j`nproc`

# Build
$ mv device/samsung/j6lte/build_skyhawk.sh .
$ . build_skyhawk.sh j6lte
```
